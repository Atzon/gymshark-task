# README 


## Public exposed endpoints

### Calculate packages
`http://ec2-13-48-1-38.eu-north-1.compute.amazonaws.com:8080/packages?items=250`

### Set package sizes
`http://ec2-13-48-1-38.eu-north-1.compute.amazonaws.com:8080/set-sizes?sizes=250,500,1000,2000,5000`
