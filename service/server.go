package service

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"example.com/service/packages"
	"github.com/gorilla/mux"
)

var pkg packages.Packages

func init() {
	pkg = packages.New()
}

func New() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/packages", packagesHandler)
	r.HandleFunc("/set-sizes", setSizesHandler)

	return r
}

func packagesHandler(w http.ResponseWriter, r *http.Request) {
	// validation
	i := r.URL.Query().Get("items")
	if i == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("error: missing 'items' query param"))
		return
	}

	items, err := strconv.Atoi(i)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("error: could not parse 'items' query param, should be integer value"))
		return
	}

	result := pkg.CalculateNumberOfPackages(items)

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Sizes: %v\n", pkg.GetPackages())
	fmt.Fprintf(w, "Items: %v\n", items)
	fmt.Fprintf(w, "Packages: %v\n", result)
}

func setSizesHandler(w http.ResponseWriter, r *http.Request) {
	// validation
	s := r.URL.Query().Get("sizes")
	if s == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("error: missing 'sizes' query param"))
		return
	}

	sizes, err := parseItems(s)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("error: could not parse 'sizes' query param, should be comma seperated integer value"))
		return
	}

	pkg.SetPackages(sizes)

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Sizes: %v\n", pkg.GetPackages())
}

func parseItems(i string) ([]int, error) {
	var result []int

	items := strings.Split(i, ",")

	for _, item := range items {
		v, err := strconv.Atoi(item)
		if err != nil {
			return nil, err
		}

		result = append(result, v)
	}

	return result, nil
}
