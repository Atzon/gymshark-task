package packages

import (
	"math"
	"sort"
)

var defaultPackages = []int{250, 5000, 500, 2000, 1000}

type Packages struct {
	packages []int
}

func New() Packages {
	sort.Sort(sort.Reverse(sort.IntSlice(defaultPackages)))

	return Packages{
		packages: defaultPackages,
	}
}

func (p *Packages) CalculateNumberOfPackages(itemsToSend int) []int {
	sum := math.MaxInt
	result := []int{}

	var calc func([]int, int, []int)
	calc = func(
		selectedPackages []int,
		sumOfselectedPackages int,
		packages []int) {

		if sumOfselectedPackages >= itemsToSend && sumOfselectedPackages < sum {
			sum = sumOfselectedPackages
			result = selectedPackages
		}

		// no more options
		if len(packages) == 0 || sumOfselectedPackages >= itemsToSend {
			return
		}

		calc(append(selectedPackages, packages[0]), sumOfselectedPackages+packages[0], packages)
		calc(selectedPackages, sumOfselectedPackages, packages[1:])
	}

	calc([]int{}, 0, p.packages)

	return result
}

func (p *Packages) SetPackages(packages []int) {
	sort.Sort(sort.Reverse(sort.IntSlice(packages)))
	p.packages = packages
}

func (p *Packages) GetPackages() []int {
	return p.packages
}
