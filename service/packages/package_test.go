package packages

import (
	"reflect"
	"testing"
)

func TestPackages_CalculateNumberOfPackages(t *testing.T) {
	tests := []struct {
		name         string
		packages     []int
		items        int
		wantPackages []int
	}{
		{
			name:         "1",
			packages:     []int{5000, 2000, 1000, 500, 250},
			items:        1,
			wantPackages: []int{250},
		},
		{
			name:         "250",
			packages:     []int{5000, 2000, 1000, 500, 250},
			items:        250,
			wantPackages: []int{250},
		},
		{
			name:         "251",
			packages:     []int{5000, 2000, 1000, 500, 250},
			items:        251,
			wantPackages: []int{500},
		},
		{
			name:         "501",
			packages:     []int{5000, 2000, 1000, 500, 250},
			items:        501,
			wantPackages: []int{500, 250},
		},
		{
			name:         "12001",
			packages:     []int{5000, 2000, 1000, 500, 250},
			items:        12001,
			wantPackages: []int{5000, 5000, 2000, 250},
		},
		{
			name:         "12001",
			packages:     []int{23, 31, 53},
			items:        263,
			wantPackages: []int{23, 23, 31, 31, 31, 31, 31, 31, 31},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Packages{
				packages: tt.packages,
			}
			if got := p.CalculateNumberOfPackages(tt.items); !reflect.DeepEqual(got, tt.wantPackages) {
				t.Errorf("CalculateNumberOfPackages() = %v, want %v", got, tt.wantPackages)
			}
		})
	}
}
